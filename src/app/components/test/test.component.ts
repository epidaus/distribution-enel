import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import PotreeData from '../../mock/potree.mock.json'

declare var window: any;
declare var Potree: any;
declare var THREE: any;
declare var $: any;
declare var viewer: any;

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  datas: any
  filteredData: any
  cam_target: any
  cam_position: any

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.datas = this.dataService.activeFile;
    this.filteredData = this.filterPotree(this.datas)

    this.renderPotree()
    
    this.dataService.onSharedFile.subscribe((data) => {
      
      this.datas = data;
      this.filteredData = this.filterPotree(this.datas)
      this.renderPotree()

      // if(this.dataService.activeFile.name !== this.datas.name){
      //   this.renderPotree()
      // }

    })
  }

  renderPotree() {


    window.viewer = new Potree.Viewer(document.getElementById("potree_render_area"));
		
    viewer.setEDLEnabled(true);
    viewer.setFOV(60);
    viewer.setPointBudget(1*1000*1000);
    document.title = "";
    viewer.setEDLEnabled(true);
    viewer.useHQ = true;
    viewer.setBackground("gradient"); // ["skybox", "gradient", "black", "white"];
    viewer.setDescription(``);
    viewer.loadSettingsFromURL();
    
    viewer.loadGUI(() => {
      viewer.setLanguage('en');
      $("#menu_appearance").next().show();
      $("#menu_tools").next().show();
      $("#menu_scene").next().show();
      //viewer.toggleSidebar();
    });
    
    var url = "https://aerodyne-enel.s3-sa-east-1.amazonaws.com/";
    const proxy:string = "https://proxy.aerodyne.dev/";
  
  Potree.loadPointCloud(proxy+url+this.filteredData.pointcloud, "index", e => {
    
let scene = viewer.scene;
scene.addPointCloud(e.pointcloud);		

this.filteredData.potree.forEach((x) => {
  
  { // HEIGHT MEASURE 1
    let measure = new Potree.Measure();
    measure.name = "Measurement" + x.name;
    measure.closed = false;
    measure.addMarker(new THREE.Vector3(
      x.distance1.x,
      x.distance1.y,
      x.distance1.z
            
     
    ));
    measure.addMarker(new THREE.Vector3(
      x.distance2.x,
      x.distance2.y,
      x.distance2.z
     
    ));
    
            
    scene.addMeasurement(measure);
  }
  
{ // HEIGHT TREE 1
    let measure = new Potree.Measure();
    measure.name = "Height" + x.name;
    measure.closed = false;
  measure.showDistance = false;
  measure.showHeight = true;
    measure.addMarker(new THREE.Vector3(
      x.height1.x,
      x.height1.y,
      x.height1.z

     
    ));
    measure.addMarker(new THREE.Vector3(
      x.height2.x,
      x.height2.y,
      x.height2.z
     
    ));

    scene.addMeasurement(measure);
   }

   scene.addAnnotation([x.annotation.x, x.annotation.y, x.annotation.z],{"cameraPosition": [x.cam_position.x, x.cam_position.y, x.cam_position.z],
            "cameraTarget": [x.cam_target.x, x.cam_target.y, x.cam_target.z],"title": x.name});
  
})

////////////////////////MEASUREMENT CODING///////////////////////////////////////////////////////////////////////////////////////
////////////////////////TOWER ANNOTATION///////////////////////////////////////////////////////////////////////////////

  
      this.filteredData.cam_position.forEach((x) => {
        this.cam_position = x
      })
// position coordinate
      viewer.scene.view.position.set(
        this.cam_position.x, this.cam_position.y, this.cam_position.z
      );
      
      this.filteredData.cam_target.forEach((x) => {
          this.cam_target = x
      })
      // target coordinate
      viewer.scene.view.lookAt(
        this.cam_target.x, this.cam_target.y, this.cam_target.z
      );

      //must delete viewer.fitToScreen()



let pointcloud = e.pointcloud;
      let material = pointcloud.material;
      material.pointColorType = Potree.PointColorType.RGB; // any Potree.PointColorType.XXXX 
      material.size = 1;
      material.pointSizeType = Potree.PointSizeType.ADAPTIVE;
      material.shape = Potree.PointShape.SQUARE;
      
    });
  }

  filterPotree(data) {
    return PotreeData.find((x) => data.name === x.name)
  }

}
