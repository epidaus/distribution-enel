import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

declare var $: any;
@Component({
  selector: 'app-card-small',
  templateUrl: './card-small.component.html',
  styleUrls: ['./card-small.component.scss'],
})
export class CardSmallComponent implements OnInit {
  datas: any;
  files: any;
  site: any;
  trees: any;
  selectedFile: any;
  bgSeverityColor: any;
  activeCard: any;
  prevIndex: number = 0;
  activeTree:any
  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    if (this.dataService.MarkerVar == undefined) {
      this.dataService.MarkerVar = this.dataService.invokePCardFunction.subscribe(
        (data) => {
          const index = this.datas.file_name.findIndex((x) => {
            return x.name === data.name;
          });
            
          // if(this.prevIndex<index&&this.prevIndex!=0){
          //   var element = document.getElementById('heading-' + (index));
          //   element.scrollIntoView({
          //     behavior: 'auto',
          //     block: 'end'
          //   });
          //   // var elmnt = document.querySelector('.card-body');
          //   // var y = elmnt.scrollTop;
          //   // console.log("scroll",y);
          //   // elmnt.scroll({behavior: 'smooth',top:(y)})
          // } else {
            var element = document.getElementById('heading-' + (index));
            element.scrollIntoView({
              behavior: 'smooth',
              block: 'start'
            });
            // this.activeTree = data.name
          // }
          // else {
            // var element = document.getElementById('heading-' + (index));
            // element.scrollIntoView({
            //   behavior: 'auto',
            //   block: 'end'
            // });
            // var elmnt = document.querySelector('.card-body');
            // var y = elmnt.scrollTop;
            // // console.log("scroll",y);
            // elmnt.scrollTo({behavior: 'smooth',top:(y-245)})
        // }
          
          // elmnt.scrollTo({top:100});
          // var y = elmnt.scrollTop;
          // console.log("scroll",y);
          // if(this.prevIndex<index){
          //   elmnt.scrollTop = y
          // }
          
          this.prevIndex = index

          // var element2 = document.getElementById("accordionExample");
          // element2.scrollTop = 500;
          // console.log(index)
        }
      );
    }

    if (this.dataService.CloseLogoPCardVar == undefined) {
      this.dataService.CloseLogoPCardVar = this.dataService.invokeLogoClosePrimaryCardFunction.subscribe(
        () => {
          this.closeAll();
          // console.log("logo clicked")
        }
      );
    }

    if (this.dataService.CloseSCVar == undefined) {
      this.dataService.CloseSCVar = this.dataService.invokeSmallCardCloseFunction.subscribe(
        () => {
          // console.log("potree close clicked");
          this.close();
        }
      );
    }

    this.dataService.onSharedId.subscribe((data) => {
      this.datas = data;
    });

    this.dataService.onSharedFile.subscribe((data) => {
      this.files = data;
      this.activeCard = data.name;
      console.log(this.activeCard);
      
      this.bgSeverityColor = data.severity;
    });

    this.dataService.onSharedTree.subscribe((data) => {
      this.trees = data;
    });

    // $(function () {
    //   $('#accordion').accordion();
    // });
  }

  // potree(data) {
  //   this.dataService.selectedFile = data
  //   // console.log("clicked potree",data)
  //   this.dataService.onCardSmallToHomeClicked(data);
  // }

  treeInfo(treeData) {
    this.dataService.selectedTree = treeData;
  }

  close() {
    this.files = undefined;
    this.trees = undefined;
    var element = document.getElementById('heading-' + this.prevIndex);
    element.scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
  }

  closeAll() {
    this.files = undefined;
    this.datas = undefined;
    this.activeCard = undefined;
  }

  onClickPrimaryBtn(file, index) {
    // var element = document.getElementById('heading-'+index)
    // element.scrollIntoView({behavior: 'smooth',block: 'center', inline: 'center' })
    var element = document.getElementById('heading-' + (index));
    element.scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
    this.prevIndex = index
    this.dataService.onSmallCardTreeClick(file);
    this.dataService.selectedFile = file;
    if (this.trees != undefined && this.trees != file) {
      this.dataService.selectedTree = file;
    }
  }
}
