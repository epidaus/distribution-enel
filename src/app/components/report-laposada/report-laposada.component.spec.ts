import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportLaposadaComponent } from '../report-laposada/report-.component';

describe('ReportLaposadaComponent', () => {
  let component: ReportLaposadaComponent;
  let fixture: ComponentFixture<ReportLaposadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportLaposadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportLaposadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
