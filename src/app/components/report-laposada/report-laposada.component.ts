// import { DataService } from 'src/app/service/data.service';
import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-report-laposada',
  templateUrl: './report-laposada.component.html',
  styleUrls: ['./report-laposada.component.scss']
})
export class ReportLaposadaComponent implements OnInit {
  @Input() siteParent: any
  sites: any

  constructor(private data: DataService) { }

  ngOnInit(): void {
    this.data.getData().subscribe((res => {

      this.sites = res.filter(item => item.site_id === 1)[0]
      console.log(this.sites)
    }))
  }

  getClass(val) {
    if (val >= 1 && val <= 10) {
      return 'Bueno'
    } else if (val >= 11 && val <= 20) {
      return 'Leve'
    } else if (val >= 21 && val <= 46) {
      return 'Critico'
    } else {
      return 'none'
    }
  }

}
