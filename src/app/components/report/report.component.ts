// import { DataService } from 'src/app/service/data.service';
import { Component, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  constructor(private data:DataService) { }

  sites: any

  ngOnInit(): void {
    this.data.getData().subscribe((res)=>{
      this.sites = res
    })
  }
}
