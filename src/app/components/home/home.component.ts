import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DataService } from 'src/app/service/data.service';

declare var MarkerWithLabel;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  center = { lat: -33.29730993693849, lng: -70.61646576031748 };
  map: google.maps.Map;
  markersFile: google.maps.Marker[] = [];
  infoWindow = new google.maps.InfoWindow();
  site: any;
  trees: any;
  check: any;
  id: any;
  dataById: any;
  filtered: any;
  lastOpenedInfoWindow: any;
  datas: any;
  unfilteredGood: any;
  unfilteredMinor: any;
  unfilteredCritical: any;
  unfilteredSinSanAlto: any;
  unfilteredSinSanMedio: any;
  unfilteredSinSanBajo: any;
  unfilteredSinSanConAlto: any;
  unfilteredSinSanConMedio: any;
  unfilteredSinSanConBajo: any;
  filtered_d: any;
  constructor(
    private data: DataService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.check = params['file_name'];
      this.id = params['site'];
      if (this.check) {
        this.dataById = this.data.getDAtabyId(this.id);
        // console.log(this.dataById);
        this.filtered = this.dataById.file_name.filter(
          (item) => item.name === this.check
        )[0];
        // console.log("this is from table view",this.filtered);

        this.data.selected = this.dataById
        this.data.selectedTree = this.filtered;
        this.data.selectedFile = this.filtered;
        
      }
    });

    this.data.onSharedId.subscribe((data) => {
      if (this.markersFile != []) {
        this.deletelMarkersFile();
      }
      this.datas = data;

      data.file_name.map((tree: any) => {
        this.addMarkerFile(tree);
      });
    });

    this.data.onSharedTree.subscribe((data) => {
      this.trees = data;
    });

    if (this.data.TreeVar == undefined) {
      this.data.TreeVar = this.data.invokeSmallCardTreeFunction.subscribe(
        (site: any) => {
          // console.log(site)
          const pos = { lat: site.lat, lng: site.lng };
          this.map.panTo(pos);
          this.map.setZoom(17);
          this.closeLastOpenedInfoWindow();
          const index = this.markersFile.findIndex(
            (x: typeof MarkerWithLabel) => {
              return x.labelContent === site.name;
            }
          );

          const infoWindow = new google.maps.InfoWindow({
            content: site.name,
          });
          this.lastOpenedInfoWindow = infoWindow;
          // infoWindowSC.setContent(site.name);
          infoWindow.open(this.map, this.markersFile[index]);
        }
      );
    }
    //home button click
    if (this.data.MapVar == undefined) {
      this.data.MapVar = this.data.invokeResetMapFunction.subscribe(() => {
        this.clearMarkersFile();
        this.map.setZoom(11);
        this.map.setCenter(this.center);
      });
    }

    if (this.data.FilterVar == undefined) {
      this.data.FilterVar = this.data.invokeFilterMapFunction.subscribe(
        (searchText) => {
          // console.log("map clicked")
          this.deletelMarkersFile();
          var filtered = this.datas.file_name.filter((it) => {
            return it.scientific_name == searchText;
          });
          filtered.map((tree: any) => {
            this.addMarkerFile(tree);
          });
        }
      );
    }
    //severity filter reset
    if (this.data.ResetVar == undefined) {
      this.data.ResetVar = this.data.invokeResetClickFunction.subscribe(() => {
        this.clearMarkersFile();
        if (this.unfilteredCritical) {
          this.unfilteredCritical.forEach((item) => {
            this.markersFile.push(item);
          });
        }
        if (this.unfilteredMinor) {
          this.unfilteredMinor.forEach((item) => {
            this.markersFile.push(item);
          });
        }
        if (this.unfilteredGood) {
          this.unfilteredGood.forEach((item) => {
            this.markersFile.push(item);
          });
        }
        // this.datas.file_name.map((tree: any) => {
        //   this.addMarkerFile(tree);
        // });
        this.map.setZoom(13);
        this.showMarkersFile();
      });
    }

    if (this.data.FilterResetMapVar == undefined) {
      this.data.FilterResetMapVar = this.data.invokeFilterResetMapFunction.subscribe(
        () => {
          this.clearMarkersFile();
          this.datas.file_name.map((tree: any) => {
            this.addMarkerFile(tree);
          });
          this.showMarkersFile();
        }
      );
    }

    if (this.data.FilterClassVar == undefined) {
      this.data.FilterClassVar = this.data.invokeFilterClassificationFunction.subscribe(
        (data) => {
          console.log('data', data);
          if (data.status == 'inactive') {
            switch (data.type) {
              case 'sinAlto':
                var filtered = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.sinsan != 'Riesgo Alto';
                  }
                );
                this.clearMarkersFile();
                this.unfilteredSinSanAlto = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.sinsan == 'Riesgo Alto';
                  }
                );
                this.markersFile = filtered;
                this.showMarkersFile();
                break;
              case 'sinMedio':
                var filtered = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.sinsan != 'Riesgo Medio';
                  }
                );
                this.clearMarkersFile();
                this.unfilteredSinSanMedio = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.sinsan == 'Riesgo Medio';
                  }
                );
                this.markersFile = filtered;
                this.showMarkersFile();
                break;
              case 'sinBajo':
                var filtered = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.sinsan != 'Riesgo Bajo';
                  }
                );
                this.clearMarkersFile();
                this.unfilteredSinSanBajo = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.sinsan == 'Riesgo Bajo';
                  }
                );
                this.markersFile = filtered;
                this.showMarkersFile();
                break;
              case 'conAlto':
                var filtered = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.sinsancon != 'Riesgo Alto';
                  }
                );
                this.clearMarkersFile();
                this.unfilteredSinSanConAlto = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.sinsancon == 'Riesgo Alto';
                  }
                );
                this.markersFile = filtered;
                this.showMarkersFile();
                break;
              case 'conMedio':
                var filtered = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.sinsancon != 'Riesgo Medio';
                  }
                );
                this.clearMarkersFile();
                this.unfilteredSinSanConMedio = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.sinsancon == 'Riesgo Medio';
                  }
                );
                this.markersFile = filtered;
                this.showMarkersFile();
                break;
              case 'conBajo':
                var filtered = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.sinsancon != 'Riesgo Bajo';
                  }
                );
                this.clearMarkersFile();
                this.unfilteredSinSanConBajo = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.sinsancon == 'Riesgo Bajo';
                  }
                );
                this.markersFile = filtered;
                this.showMarkersFile();
                break;
            }
          } else {
            switch (data.type) {
              case 'sinAlto':
                this.clearMarkersFile();
                this.unfilteredSinSanAlto.forEach((item) => {
                  this.markersFile.push(item);
                });
                this.showMarkersFile();
                break;
              case 'sinMedio':
                this.clearMarkersFile();
                this.unfilteredSinSanMedio.forEach((item) => {
                  this.markersFile.push(item);
                });
                this.showMarkersFile();
                break;
              case 'sinBajo':
                this.clearMarkersFile();
                this.unfilteredSinSanBajo.forEach((item) => {
                  this.markersFile.push(item);
                });
                this.showMarkersFile();
                break;
              case 'conAlto':
                this.clearMarkersFile();
                this.unfilteredSinSanConAlto.forEach((item) => {
                  this.markersFile.push(item);
                });
                this.showMarkersFile();
                break;
              case 'conMedio':
                this.clearMarkersFile();
                this.unfilteredSinSanConMedio.forEach((item) => {
                  this.markersFile.push(item);
                });
                this.showMarkersFile();
                break;
              case 'conBajo':
                this.clearMarkersFile();
                this.unfilteredSinSanConBajo.forEach((item) => {
                  this.markersFile.push(item);
                });
                this.showMarkersFile();
                break;
            }
          }
        }
      );
    }

    if (this.data.FilterSeverityVar == undefined) {
      this.data.FilterSeverityVar = this.data.invokeFilterSeverityFunction.subscribe(
        (data) => {
          if (data.status == 'inactive') {
            switch (data.severity) {
              case 'Good':
                var filtered = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.icon != 'assets/icons/tree-icon-Bueno@40px.svg';
                  }
                );
                this.clearMarkersFile();
                this.unfilteredGood = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.icon == 'assets/icons/tree-icon-Bueno@40px.svg';
                  }
                );
                this.markersFile = filtered;
                this.showMarkersFile();
                break;
              case 'Minor':
                var filtered = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.icon != 'assets/icons/tree-icon-Leve@40px.svg';
                  }
                );
                this.unfilteredMinor = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return item.icon == 'assets/icons/tree-icon-Leve@40px.svg';
                  }
                );
                this.clearMarkersFile();
                this.markersFile = filtered;
                this.showMarkersFile();
                break;
              case 'Critical':
                var filtered = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return (
                      item.icon != 'assets/icons/tree-icon-Critico@40px.svg'
                    );
                  }
                );
                this.clearMarkersFile();
                this.unfilteredCritical = this.markersFile.filter(
                  (item: typeof MarkerWithLabel) => {
                    return (
                      item.icon == 'assets/icons/tree-icon-Critico@40px.svg'
                    );
                  }
                );
                this.markersFile = filtered;
                console.log('Critico clicked', filtered);
                this.showMarkersFile();
                break;
            }
          } else {
            switch (data.severity) {
              case 'Good':
                this.clearMarkersFile();
                this.unfilteredGood.forEach((item) => {
                  this.markersFile.push(item);
                });
                this.showMarkersFile();
                break;
              case 'Minor':
                this.clearMarkersFile();
                this.unfilteredMinor.forEach((item) => {
                  this.markersFile.push(item);
                });
                this.showMarkersFile();
                break;
              case 'Critical':
                this.clearMarkersFile();
                this.unfilteredCritical.forEach((item) => {
                  this.markersFile.push(item);
                });
                this.showMarkersFile();
                break;
            }
          }
        }
      );
    }
  }

  ngAfterViewInit(): void {
    this.initMap();
  }

  initMap(): void {
    const env = this;

    this.map = new google.maps.Map(
      document.getElementById('map') as HTMLElement,
      {
        center: new google.maps.LatLng(this.center.lat, this.center.lng),
        disableDefaultUI: true,
        fullscreenControl: false,
        mapTypeControl: false,
        streetViewControl: false,
        zoom: 11,
        styles: [
          {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [
              {
                color: '#e9e9e9',
              },
              {
                lightness: 17,
              },
            ],
          },
          {
            featureType: 'landscape',
            elementType: 'geometry',
            stylers: [
              {
                color: '#f5f5f5',
              },
              {
                lightness: 20,
              },
            ],
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry.fill',
            stylers: [
              {
                color: '#ffffff',
              },
              {
                lightness: 17,
              },
            ],
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [
              {
                color: '#ffffff',
              },
              {
                lightness: 29,
              },
              {
                weight: 0.2,
              },
            ],
          },
          {
            featureType: 'road.arterial',
            elementType: 'geometry',
            stylers: [
              {
                color: '#ffffff',
              },
              {
                lightness: 18,
              },
            ],
          },
          {
            featureType: 'road.local',
            elementType: 'geometry',
            stylers: [
              {
                color: '#ffffff',
              },
              {
                lightness: 16,
              },
            ],
          },
          {
            featureType: 'poi',
            elementType: 'geometry',
            stylers: [
              {
                color: '#f5f5f5',
              },
              {
                lightness: 21,
              },
            ],
          },
          {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [
              {
                color: '#dedede',
              },
              {
                lightness: 21,
              },
            ],
          },
          {
            elementType: 'labels.text.stroke',
            stylers: [
              {
                visibility: 'on',
              },
              {
                color: '#ffffff',
              },
              {
                lightness: 16,
              },
            ],
          },
          {
            elementType: 'labels.text.fill',
            stylers: [
              {
                saturation: 36,
              },
              {
                color: '#333333',
              },
              {
                lightness: 40,
              },
            ],
          },
          {
            elementType: 'labels.icon',
            stylers: [
              {
                visibility: 'off',
              },
            ],
          },
          {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [
              {
                color: '#f2f2f2',
              },
              {
                lightness: 19,
              },
            ],
          },
          {
            featureType: 'administrative',
            elementType: 'geometry.fill',
            stylers: [
              {
                color: '#fefefe',
              },
              {
                lightness: 20,
              },
            ],
          },
          {
            featureType: 'administrative',
            elementType: 'geometry.stroke',
            stylers: [
              {
                color: '#fefefe',
              },
              {
                lightness: 17,
              },
              {
                weight: 1.2,
              },
            ],
          },
        ],
      }
    );

    //Load KML
    this.data.getData().subscribe((sites) => {
      console.log('KML Sites: ', sites);
      sites.forEach((site) => {
        const kmlLayer = new google.maps.KmlLayer({
          url: site.kml,
          suppressInfoWindows: true,
          preserveViewport: true,
        });
        kmlLayer.setMap(this.map);
        kmlLayer.addListener('click', function (kmlEvent) {
          var pos = { lat: site.lat, lng: site.lng };
          env.data.selected = site;
          // console.log(env.trees);
          env.map.panTo(pos);
          env.map.setZoom(13);
          env.showMarkersFile();
          // env.data.onSmallCardCloseClick();
        });
        const firstTreeLatLng: google.maps.LatLng = new google.maps.LatLng(
          site.file_name[0].lat,
          site.file_name[0].lng
        );
        const siteMarker = new MarkerWithLabel({
          map: this.map,
          position: firstTreeLatLng,
          icon: 'assets/icons/Icon-Transparent.png',
          labelContent: site.file_name[0].site_name,
          labelAnchor: new google.maps.Point(40, -10),
          labelClass: 'tree-icon-label',
          labelInBackground: false,
          scaledSize: new google.maps.Size(50, 50),
        });

        siteMarker.addListener('click', (e) => {
          google.maps.event.trigger(kmlLayer, 'click');
        });
      });
    });
  }

  addMarkerFile(point: any) {
    var pos = { lat: point.lat, lng: point.lng };
    // console.log(point.severity);

    const infoWindow = new google.maps.InfoWindow();
    infoWindow.setContent(point.name);
    // const markerFile = new google.maps.Marker({
    //   position: pos,
    //   map: this.map,
    //   icon: {
    //     labelOrigin: new google.maps.Point(76, 43),
    //     url: 'assets/icons/tree-icon-'+point.severity+'.svg',
    //     scaledSize: new google.maps.Size(50, 50),
    //   },
    //   label: {
    //     text: point.name,
    //     color: '#2D2A2A',
    //     fontWeight: 'bold',
    //     fontSize: '16px',
    //   },
    // });

    const markerFile = new MarkerWithLabel({
      map: this.map,
      position: pos,
      icon: 'assets/icons/tree-icon-' + point.severity + '@40px.svg',
      labelContent: point.name,
      labelAnchor: new google.maps.Point(40, -10),
      labelClass: 'tree-icon-label',
      labelInBackground: false,
      scaledSize: new google.maps.Size(50, 50),
      sinsan: point.risk_classification_wo_health,
      sinsancon: point.risk_classification_w_health,
    });

    markerFile.addListener('click', () => {
      console.log(point);
      // console.log(point);
      this.data.onPCardFunctionClick(point);
      this.data.selectedFile = point;
      // this.data.selectedTree = point;
      const pos = { lat: point.lat, lng: point.lng };
      // this.setMapOnPoint(null);
      this.map.setZoom(15);
      this.map.panTo(pos);
      this.closeLastOpenedInfoWindow();
      this.lastOpenedInfoWindow = infoWindow;
      infoWindow.open(this.map, markerFile);
      // console.log(markerFile);
    });
    markerFile.addListener('mouseover', () => {
      this.closeLastOpenedInfoWindow();
      this.lastOpenedInfoWindow = infoWindow;
      infoWindow.open(this.map, markerFile);
    });
    // markerFile.addListener('mouseout', () => {
    //   infoWindow.close();
    // });
    this.markersFile.push(markerFile);
  }

  setMapOnFile(map: google.maps.Map | null) {
    for (let i = 0; i < this.markersFile.length; i++) {
      this.markersFile[i].setMap(map);
    }
  }

  showMarkersFile() {
    this.setMapOnFile(this.map);
  }

  clearMarkersFile() {
    this.setMapOnFile(null);
  }

  deletelMarkersFile() {
    this.clearMarkersFile();
    this.markersFile = [];
  }
  closeLastOpenedInfoWindow() {
    if (this.lastOpenedInfoWindow) {
      this.lastOpenedInfoWindow.close();
    }
  }
}
