import { DataService } from 'src/app/service/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.scss']
})
export class AnalysisComponent implements OnInit {

  file: any;

  constructor(private data:DataService) { }

  ngOnInit(): void {
    this.data.onSharedFile.subscribe((data)=>{
      this.file = data
    })

    if(this.data.LogoToAnalysisVar == undefined) {
      this.data.LogoToAnalysisVar = this.data.invokeLogoCloseAnalysis.subscribe(()=>{
        this.file = undefined
      })
    }
  }

}
