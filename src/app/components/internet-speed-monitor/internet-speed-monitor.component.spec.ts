import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternetSpeedMonitorComponent } from './internet-speed-monitor.component';

describe('InternetSpeedMonitorComponent', () => {
  let component: InternetSpeedMonitorComponent;
  let fixture: ComponentFixture<InternetSpeedMonitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternetSpeedMonitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternetSpeedMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
