import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { DataService } from './../../service/data.service';

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.scss'],
})
export class TableListComponent implements OnInit {
  projectID: any;
  dataList: any;
  dtOptions: DataTables.Settings = {};
  constructor(
    private activatedRoute: ActivatedRoute,
    private dataService: DataService
  ) {}

  ngOnInit(): void {

    this.activatedRoute.params.subscribe(({ id }) => {
      this.projectID = id;
      this.dataList = this.dataService.getDAtabyId(id);
    });

    this.dtOptions = {
      pagingType: 'full_numbers',
      responsive:true,
    };
  }
}
