import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private data: DataService, private fb: FormBuilder, public router: Router) { }

  ngOnInit(): void {
  }

  loginForm = this.fb.group({
    username: [''],
    password: ['']
  })

  login() {
    this.data.getUserInfo(this.loginForm.value).subscribe((res) => {
      // console.log(res);
      if (res) {
        localStorage.setItem('token', res.token)
        this.router.navigate(['home']);
      }
    })
  }

}
