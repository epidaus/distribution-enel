import { DataService } from 'src/app/service/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {


  constructor(private data:DataService) { }

  ngOnInit(): void {
    
  }

}
