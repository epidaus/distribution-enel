import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { GoogleMapsModule } from '@angular/google-maps';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JwtModule, JwtModuleOptions } from '@auth0/angular-jwt';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { SpeedTestModule } from 'ng-speed-test';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { CardMediumComponent } from './components/card-medium/card-medium.component';
import { CardSmallComponent } from './components/card-small/card-small.component';
import { LogoComponent } from './components/logo/logo.component';
import { LogoutButtonComponent } from './components/logout-button/logout-button.component';
import { LoginComponent } from './components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GalleryComponent } from './components/gallery/gallery.component';
import { TestComponent } from './components/test/test.component';
import { CardIconComponent } from './components/card-icon/card-icon.component';
import { ReportComponent } from './components/report/report.component';
import { ReportCoverComponent } from './components/report-cover/report-cover.component';
import { ReportLaposadaComponent } from './components/report-laposada/report-laposada.component';
import { ReportTreeComponent } from './components/report-algarrobal/report-algarrobal.component';
import { PotreeViewerComponent } from './components/potree-viewer/potree-viewer.component';
import { TableViewComponent } from './components/table-view/table-view.component';
import { InternetSpeedMonitorComponent } from './components/internet-speed-monitor/internet-speed-monitor.component';
import { TableListComponent } from './components/table-list/table-list.component';
import { ImageViewerModule } from '@emazv72/ngx-imageviewer';
import { FilterSpeciesPipe } from './pipes/filter-species.pipe';
import { AnalysisComponent } from './components/analysis/analysis.component';
import { DataTablesModule } from 'angular-datatables';

export function tokenGetter() {
  return localStorage.getItem("token");
}

const JWT_Module_Options: JwtModuleOptions = {
  config: {
    tokenGetter: tokenGetter
  },
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CardMediumComponent,
    CardSmallComponent,
    LogoComponent,
    LogoutButtonComponent,
    LoginComponent,
    GalleryComponent,
    TestComponent,
    CardIconComponent,
    ReportComponent,
    ReportCoverComponent,
    ReportLaposadaComponent,
    ReportTreeComponent,
    PotreeViewerComponent,
    TableViewComponent,
    InternetSpeedMonitorComponent,
    TableListComponent,
    FilterSpeciesPipe,
    AnalysisComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GoogleMapsModule,
    FormsModule,
    ReactiveFormsModule,
    JwtModule.forRoot(JWT_Module_Options),
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    SpeedTestModule,
    ImageViewerModule,
    DataTablesModule
  ],
  providers: [BsModalRef],
  entryComponents: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
