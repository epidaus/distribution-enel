import { ReportLaposadaComponent } from './components/report-laposada/report-laposada.component';
import { ReportComponent } from './components/report/report.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component'
import { LoginComponent } from './components/login/login.component'
import { GalleryComponent } from './components/gallery/gallery.component'
import { TestComponent } from './components/test/test.component'
import { AuthGuardService as AuthGuard } from './service/auth-guard.service';
import { TableViewComponent } from './components/table-view/table-view.component';
import { TableListComponent } from './components/table-list/table-list.component'
import { ReportTreeComponent } from './components/report-algarrobal/report-algarrobal.component'

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard],
    children: [
      {
      path: 'tableView',
      component: TableViewComponent,
      children: [
        {
          path: 'tableList/:id',
          component: TableListComponent
        }
      ]
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'gallery',
    component: GalleryComponent,
    canActivate: [AuthGuard]
  },
  {
      path: 'report1',
      component: ReportLaposadaComponent
  },
  {
    path: 'report2',
    component: ReportTreeComponent
  }
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
